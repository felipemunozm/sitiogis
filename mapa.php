<!DOCTYPE HTML>
<html lang="es">
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<style type="text/css">
  html { height: 100% }
  body { height: 100%; margin: 0px; padding: 0px }
  #map_canvas { height: 100% }
</style>
<script type="text/javascript"
    src="https://maps.google.com/maps/api/js?sensor=false">
</script>
<script type="text/javascript">
  function initialize() {
<?php
    include('config.php');
    $tabla=mysql_query("select Latitud,Longitud,Nombre_localidad from Localidad where Id_localidad=".$_GET['places']);
    $row=mysql_fetch_array($tabla);
    echo "var lat=".$row['Latitud'].";";
    echo "var lng=".$row['Longitud'].";";
    mysql_free_result($tabla);
    $lat=$row['Latitud'];
    $lng=$row['Longitud'];
?>
    var directionsService = new google.maps.DirectionsService();
    var directionsDisplay = new google.maps.DirectionsRenderer();
    var place= new google.maps.LatLng(lat,lng);
    var santiago = new google.maps.LatLng(-33.458083,-70.657368);
    var myOptions = {
      zoom: 10,
      center: santiago,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
    directionsDisplay.setMap(map);
    var request = {
        origin:santiago,
        destination:place,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService.route(request,function(result,status){
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(result);
        }
    });
    var markersArray = [];
<?php
    $tabla2=mysql_query("select Nombre,Latitud,Longitud from Establecimiento where Latitud between least(-33.458083,".$row['Latitud'].") and greatest(".$row['Latitud'].",-33.458083) and Longitud between least(-70.657368,".$row['Longitud'].") and greatest(".$row['Longitud'].",-70.657368)");
    while($row2=mysql_fetch_array($tabla2)) {
        echo "markersArray.push(
            new google.maps.Marker({
                position: new google.maps.LatLng(".$row2['Latitud'].",".$row2['Longitud']."),
                title: ".'"'.$row2['Nombre'].'"'."
        })
        );";
    }
?>
    for (var i=0;i<markersArray.length;i++){
        markersArray[i].setMap(map);
    }
    var marker = new google.maps.Marker({
        position: place,
        title: <?php echo '"'.$row['Nombre_localidad'].'"' ?>
    });
    var santiagoMarker = new google.maps.Marker({
        position: santiago,
        title: "Santiago"
    });
    santiagoMarker.setMap(map);
    marker.setMap(map);
    }

</script>
	<title></title>
</head>
<body onload="initialize()">
     <div id="map_canvas" style="width:100%; height:100%"></div>
</body>
</html>
